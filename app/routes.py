# pylint: disable=missing-function-docstring, import-error, line-too-long

import base64
import hashlib
import json
import logging
import os
import random
import re
import smtplib
import ssl
import string
from datetime import datetime
from email.message import EmailMessage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import requests
from flask import (Flask, flash, make_response, redirect, render_template,
                   request, session, url_for, jsonify)
from flask_login import (LoginManager, UserMixin, current_user, login_required,
                         login_user, logout_user)
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.mysql import LONGTEXT
from sqlalchemy import create_engine, text

from werkzeug.security import check_password_hash, generate_password_hash

import folium
from folium.plugins import MarkerCluster
import jinja2

from app import app

blacklist_file = open('./app/blacklist.txt')
blacklist_email = [line.replace("\n", "") for line in blacklist_file.readlines()]

logger = logging.getLogger(__name__)

DEBUG = True if os.getenv("DEBUG") == "1" else False

MYSQL_HOST = os.getenv("MYSQL_HOST") or "mysql"
MYSQL_PORT = os.getenv("MYSQL_PORT") or 3306
MYSQL_DATABASE = os.getenv("MYSQL_DATABASE")
MYSQL_USER = os.getenv("MYSQL_USER")
MYSQL_PASSWORD = os.getenv("MYSQL_PASSWORD")

URL_BASE = os.getenv("URL_BASE")
MAIL_SENDER = os.getenv("MAIL_SENDER")
MAIL_PASSWORD = os.getenv("MAIL_PASSWORD")

ADMIN_USERNAME = os.getenv("ADMIN_USERNAME")
ADMIN_PASSWORD = os.getenv("ADMIN_PASSWORD")
ADMIN_EMAIL = os.getenv("ADMIN_EMAIL")
SMTP_URI = 'smtp.gmail.com'

DEFAULT_ALERT_EMAIL = os.getenv("DEFAULT_ALERT_EMAIL", "furmap.france@gmail.com")

GOOGLE_API_KEY = os.getenv("GOOGLE_API_KEY", "No_Key_PrOviDeD")

LOCK_FILE = './app/.lock'

app.debug = DEBUG
DB_NAME = f"mysql+mysqlconnector://{MYSQL_USER}:{MYSQL_PASSWORD}@{MYSQL_HOST}:{MYSQL_PORT}/{MYSQL_DATABASE}?charset=utf8mb4&collation=utf8mb4_general_ci"
app.config['SQLALCHEMY_DATABASE_URI'] = DB_NAME
app.config['SECRET_KEY'] = os.getenv("SECRET_KEY")

db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.login_view = 'login'
login_manager.login_message = "Vous devez être connecté pour continuer !"
login_manager.init_app(app)

class User(UserMixin, db.Model):
    """ User model """
    id = db.Column(db.Integer, primary_key=True)

    username = db.Column(db.String(100))
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))

    is_admin = db.Column(db.Boolean, default=False)

    description = db.Column(LONGTEXT, nullable=True)

    profile_picture = db.Column(LONGTEXT, nullable=True)

    telegram = db.Column(db.String(100), nullable=True)
    discord = db.Column(db.String(100), nullable=True)
    twitter = db.Column(db.String(100), nullable=True)
    twitternsfw = db.Column(db.String(100), nullable=True)
    furaffinity = db.Column(db.String(100), nullable=True)

    is_verified = db.Column(db.Boolean, default=False)
    email_token = db.Column(db.String(100), nullable=True)
    is_restricted = db.Column(db.Boolean, default=True)
    is_blacklisted = db.Column(db.Boolean, default=False)

    date_register = db.Column(db.Date, default=datetime.now)

    instagram = db.Column(db.String(100), nullable=True)
    bluesky = db.Column(db.String(100), nullable=True)
    youtube = db.Column(db.String(100), nullable=True)
    tiktok = db.Column(db.String(100), nullable=True)
    twitch = db.Column(db.String(100), nullable=True)
    linktree = db.Column(db.String(100), nullable=True)

    is_suiter = db.Column(db.Boolean, default=False)
    is_artist = db.Column(db.Boolean, default=False)
    has_suit = db.Column(db.Boolean, default=False)

    date_last_login = db.Column(db.Date, default=datetime.now)

    def __str__(self):
        return str(self.id) + " " + str(self.username)

class Association(db.Model):
    """ User model """
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(100), nullable=True)
    contact = db.Column(db.String(100), nullable=True)
    contact_type = db.Column(db.String(100), nullable=True)

    lat = db.Column(db.String(100))
    lon = db.Column(db.String(100))

    is_visible = db.Column(db.Boolean, default=True)


class Places(db.Model):
    """ Places model """
    id = db.Column(db.Integer, primary_key=True)

    lat = db.Column(db.String(100))
    lon = db.Column(db.String(100))

    is_visible = db.Column(db.Boolean, default=True)

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=True)


@app.context_processor
def inject_template_scope():
    injections = dict()

    def cookies_check():
        value = request.cookies.get('cookie_consent')
        if value is None:
            return value == 'true'
        return True
    injections.update(cookies_check=cookies_check)

    return injections

def random_string(length):
    pool = string.ascii_letters + string.digits
    return ''.join(random.choice(pool) for _ in range(length))


def mail_sender(content, plain, recipient, subject):

    msg = MIMEMultipart('alternative')
    msg['From'] = MAIL_SENDER
    msg['To'] = recipient
    msg['Subject'] = subject

    mimed_html = MIMEText(content, 'html')
    mimed_plain = MIMEText(plain, 'plain')

    msg.attach(mimed_plain)
    msg.attach(mimed_html)

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(SMTP_URI, 465, context=context) as server:
        server.login(MAIL_SENDER, MAIL_PASSWORD)
        server.sendmail(MAIL_SENDER, recipient, msg.as_string())
        server.quit()


@login_manager.user_loader
def load_user(user_id):
    user_connected = User.query.get(int(user_id))
    user_connected.date_last_login = datetime.now()
    db.session.commit()
    return user_connected

@app.route('/initdb')
def initdb():
    if DEBUG:
        db.create_all()
    return redirect(url_for('home'))


@app.route('/inituser')
def inituser():
    if DEBUG:
        new_user = User(
            username=ADMIN_USERNAME,
            email="admin@admin.fr",
            password=generate_password_hash(ADMIN_PASSWORD, method='sha256'),
            is_admin=True,
            is_verified=True,
            is_restricted=False
        )
        db.session.add(new_user)
        db.session.commit()
    return redirect(url_for('home'))


def get_user():
    _user_id = None
    if "_user_id" in session:
        _user_id = session['_user_id']
    return _user_id


@app.route('/register')
def register():
    return render_template('register.html', user_id=None, locked=os.path.isfile(LOCK_FILE))


@app.route('/register', methods=['POST'])
def register_post():

    username = request.form.get('username').strip()
    email = request.form.get('email').strip()
    password = request.form.get('password').strip()
    confirmation = request.form.get('confirmation').strip()
    overide_dbl = request.form.get('dbl')

    if os.path.isfile(LOCK_FILE):
        return render_template('register.html', username=username, email=email, locked=os.path.isfile(LOCK_FILE))

    url = "https://gitlab.com/GuillaumeKoenigTncy/furmap/-/raw/main/app/words_black_list.txt"
    s = requests.get(url).content
    words_black_list = []
    for elem in s.decode('utf-8').split("\n"):
        if len(elem.strip()) > 0:
            words_black_list.append(elem.strip())

    if next((s for s in words_black_list if s in username), None):
        flash("Votre pseudonyme contient une chaine de caractère invalide !")
        return render_template('register.html', username="", email=email)

    if next((s for s in words_black_list if s in email), None):
        flash("Votre email contient une chaine de caractère invalide !")
        return render_template('register.html', username=username, email="")

    user = User.query.filter_by(email=email).first()
    if user:
        flash("Cette adresse email est déjà enregstrée !")
        return render_template('register.html', username=username, email=email)

    user = User.query.filter_by(username=username).all()

    if user and not overide_dbl:
        used_mail = []
        for x in user:
            used_mail.append(x.email.replace(x.email.split("@")[0][int(len(x.email.split("@")[0])/3):-int(len(x.email.split("@")[0])/3)], "_____" ))
        flash(f"Ce pseudonyme est déjà enregstré avec le(s) mail(s) suivant(s) :  {' '.join(used_mail)} !")
        return render_template('register.html', username=username, email=email, password=password, confirmation=confirmation, dbl=True)

    if email.split("@")[1] in blacklist_email:
        flash("Ce domaine email n'est pas autorisé !")
        return render_template('register.html', username=username, email=email)

    if confirmation != password:
        flash("Les mots de passes ne sont pas identiques.")
        return render_template('register.html', username=username, email=email)
    if len(password) < 8:
        flash("Le mot de passe doit contenir au moins 8 caractères.")
        return render_template('register.html', username=username, email=email)
    if re.search('\d', password) is None:
        flash("Le mot de passe doit contenir au moins 1 minuscule, 1 majuscule et 1 chiffre.")
        return render_template('register.html', username=username, email=email)
    if re.search('[A-Z]', password) is None:
        flash("Le mot de passe doit contenir au moins 1 minuscule, 1 majuscule et 1 chiffre.")
        return render_template('register.html', username=username, email=email)

    email_token = hashlib.md5(random_string(64).encode()).hexdigest()

    new_user = User(
        username=username,
        email=email,
        email_token=email_token,
        password=generate_password_hash(password, method='sha256')
    )
    db.session.add(new_user)
    db.session.commit()

    content = f"""
<html>
    <head>
    </head>
    <body border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#e37b46" style="background: rgb(2,0,36); background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(109,13,125,1) 40%, rgba(10,59,131,1) 100%); padding:20px; color:white; ">
        <h1>Bonjour {username}</h1>
        <p>Afin de finaliser ton enregistrement il faut valider ton adresse email. Cette étapes est importante car elle te permet d'accèder à la carte des membres.</p>
        <p>Il te suffit de cliquer ici : <a href="{URL_BASE}{url_for('verify', user_id=new_user.id, email_token=email_token)}"> Click me !</a></p>
        <p>Cordialement, l'équipe FurMap !</p>
    </body>
</html>
"""

    plain = f"""
Bonjour {username},
Afin de finaliser ton enregistrement il faut valider ton adresse email. Cette étapes est importante car elle te permet d'accèder à la carte des membres.
Il te suffit de cliquer ici : {URL_BASE}{url_for('verify', user_id=new_user.id, email_token=email_token)}
Cordialement, l'équipe FurMap !
"""

    mail_sender(content, plain, email, "FurMap.fr - Vérification de votre compte !")

    login_user(new_user)
    return redirect(url_for('furmap'))


@app.route('/verify/<int:user_id>/<string:email_token>')
def verify(user_id, email_token):

    url = "https://gitlab.com/GuillaumeKoenigTncy/furmap/-/raw/main/app/email_white_list.txt"
    s = requests.get(url).content
    email_white_list = []
    for elem in s.decode('utf-8').split("\n"):
        if len(elem.strip()) > 0:
            email_white_list.append(elem.strip())

    user = User.query.filter_by(id=user_id).first()
    if user.email_token == email_token:
        if user.email.split('@')[1] not in email_white_list:
            content = f"""
<html>
    <head>
    </head>
    <body border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#e37b46" style="background: rgb(2,0,36); background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(109,13,125,1) 40%, rgba(10,59,131,1) 100%); padding:20px; color:white; ">
        <h1>Bonjour Administrateur !</h1>
        <p>Une nouvelle inscription nécessite votre attention car son email n'est pas considéré comme safe.</p>
        <p>Il s'agit de {user.username} - {user.email}</p>
    </body>
</html>
        """

            plain = f"""
Bonjour Administrateur !
Une nouvelle inscription nécessite votre attention car son email n'est pas considéré comme safe.
Il s'agit de {user.username} - {user.email}
"""

            mail_sender(content, plain, DEFAULT_ALERT_EMAIL, "FurMap.fr - Une inscription nécessite votre attention !")
        else:
            user.is_verified = True
            user.is_restricted = False
        db.session.commit()
    else:
        user.is_verified = False
        user.is_restricted = True
        db.session.commit()

    return render_template('verify.html', user_id=get_user(), verified=user.is_verified)


@app.route('/login')
def login():
    return render_template('login.html', user_id=None)


@app.route('/login', methods=['POST'])
def login_post():

    email = request.form.get('email')
    password = request.form.get('password')
    user = User.query.filter_by(email=email).first()

    if not user:
        flash("Cette adresse email n'est pas enregstrée !")
        return redirect(url_for('login'))
    if not check_password_hash(user.password, password):
        flash("Le mot de passe ne correspond pas !")
        return redirect(url_for('login'))
    if user.is_blacklisted:
        flash("Vous ne pouvez plus vous connecter. Contactez l'administrateur !")
        return redirect(url_for('login'))

    login_user(user)
    return redirect(url_for('furmap'))


@app.route('/forget_password')
def forget_password():
    return render_template('forget_password.html', user_id=None)


@app.route('/forget_password', methods=['POST'])
def forget_password_post():

    email = request.form.get('email')
    user = User.query.filter_by(email=email).first()

    if not user:
        flash("Cette adresse email n'est pas enregstrée !")
        return redirect(url_for('forget_password'))

    password = user.password
    reset_token = hashlib.md5(password.encode()).hexdigest()

    content = f"""
<html>
    <head>
    </head>
    <body border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#e37b46" style="background: rgb(2,0,36); background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(109,13,125,1) 40%, rgba(10,59,131,1) 100%); padding:20px; color:white; ">
        <h1>Bonjour {user.username}</h1>
        <p>Tu as perdu ton mot de passe pour accèder à ton compte FurMap.fr !</p>
        <p>Il te suffit de cliquer ici pour le réinitialiser : <a href="{URL_BASE}{url_for('reset_password', user_id=user.id, reset_token=reset_token)}" Click me ! </a></p>
        <p>Cordialement, l'équipe FurMap !</p>
    </body>
</html>
"""

    plain = f"""
Bonjour {user.username},
Tu as perdu ton mot de passe pour accèder à ton compte FurMap.fr !
Il te suffit de cliquer ici pour le réinitialiser : {URL_BASE}{url_for('reset_password', user_id=user.id, reset_token=reset_token)}
Cordialement, l'équipe FurMap !
"""

    mail_sender(content, plain, email, "FurMap.fr - Réinitialisation de votre mot de passe")

    flash("Email envoyé !")

    return redirect(url_for('forget_password'))


@app.route('/reset_password/<int:user_id>/<string:reset_token>')
def reset_password(user_id, reset_token):

    user = User.query.filter_by(id=user_id).first()

    password = user.password
    controled_token = hashlib.md5(password.encode()).hexdigest()

    if controled_token != reset_token:
        flash("Ce jeton n'est pas valide ! Impossible de continuer !")
        return render_template('reset_password.html', user_id=None, invalid=True)

    return render_template('reset_password.html', user_id=None, invalid=False)


@app.route('/reset_password/<int:user_id>/<string:reset_token>', methods=['POST'])
def reset_password_post(user_id, reset_token):

    user = User.query.filter_by(id=user_id).first()

    password = user.password
    controled_token = hashlib.md5(password.encode()).hexdigest()

    if controled_token != reset_token:
        flash("Ce jeton n'est pas valide ! Impossible de continuer !")
        render_template('reset_password.html', user_id=None, invalid=True)

    password = request.form.get('password')
    confirmation = request.form.get('confirmation')

    if confirmation != password:
        flash("Les mots de passes ne sont pas identiques.")
        return render_template('reset_password.html', user_id=None, invalid=False)
    if len(password) < 8:
        flash("Le mot de passe doit contenir au moins 8 caractères.")
        return render_template('reset_password.html', user_id=None, invalid=False)
    if re.search('\d', password) is None:
        flash("Le mot de passe doit contenir au moins 1 minuscule, 1 majuscule et 1 chiffre.")
        return render_template('reset_password.html', user_id=None, invalid=False)
    if re.search('[A-Z]', password) is None:
        flash("Le mot de passe doit contenir au moins 1 minuscule, 1 majuscule et 1 chiffre.")
        return render_template('reset_password.html', user_id=None, invalid=False)

    user.password = generate_password_hash(password, method='sha256')
    db.session.commit()

    return redirect(url_for('login'))


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home', user_id=get_user()))


@app.route('/profile')
@login_required
def profile():
    places = Places.query.filter_by(user_id=current_user.id).all() or []

    folium_map = folium.Map(
        location=[48.692054, 6.184417],
        zoom_start=5,
        min_zoom=5,
        max_zoom=15
    )

    marker_cluster = MarkerCluster(name="Furries").add_to(folium_map)

    for place in places:
        folium.Marker(
            location=[float(place.lat), float(place.lon)],
            icon = folium.DivIcon(html=(
            """
                <div class="pin"><b class="pin-text">"""+str(place.id)+"""</b></div>
            """
            )),

        ).add_to(marker_cluster)

    ela = folium.MacroElement().add_to(folium_map)
    ela._template = jinja2.Template("""
        {% macro header(this, kwargs) %}
            <style>
                .pin {
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    width:40px;
                    height:40px;
                    background-color:lightcoral;
                    color: black;
                    padding:3px;
                    border:1px solid black;
                    border-radius:20px 20px 0px 20px;
                    font-size: large; 
                    transform: rotate(45deg);
                    margin-top: -40px;
                    margin-left: -20px;
                }
                .pin-text {
                    transform: rotate(-45deg);
                    flex: 0 0;
                }
            </style>
        {% endmacro %}
    """)

    folium_map = folium_map._repr_html_()

    return render_template('profile.html', page="profile", user=current_user, map=folium_map, places=places)


@app.route('/profile', methods=['POST'])
@login_required
def profile_post():
    if "submit-profile" in request.form:
        telegram = request.form.get('telegram')
        discord = request.form.get('discord')
        twitter = request.form.get('twitter')
        twitternsfw = request.form.get('twitternsfw')
        furaffinity = request.form.get('furaffinity')

        instagram = request.form.get('instagram')
        bluesky = request.form.get('bluesky')
        youtube = request.form.get('youtube')
        tiktok = request.form.get('tiktok')
        twitch = request.form.get('twitch')
        linktree = request.form.get('linktree')

        current_user.is_artist = True if "artist" in request.form else False
        current_user.is_suiter = True if "maker" in request.form else False
        current_user.has_suit = True if "fursuit" in request.form else False

        current_user.telegram = telegram.replace("@", "").replace("https://t.me/", "").strip()
        current_user.discord = discord.replace("@", "").strip()
        current_user.twitter = twitter.replace("@", "").replace("https://twitter.com/", "").strip()
        current_user.twitternsfw = twitternsfw.replace("@", "").replace("https://twitter.com/", "").strip()
        current_user.furaffinity = furaffinity.replace("@", "").replace("https://furaffinity.net/user/", "").replace("https://www.furaffinity.net/user/", "").strip()

        current_user.instagram = instagram.replace("@", "")
        current_user.bluesky = bluesky.replace("@", "")
        current_user.youtube = youtube.replace("", "")
        current_user.tiktok = tiktok.replace("", "")
        current_user.twitch = twitch.replace("@", "")
        current_user.linktree = linktree.replace("@", "")

        db.session.commit()
    elif "submit-desc" in request.form:
        description = request.form.get('description')
        current_user.description = description
        db.session.commit()
    elif "submit-email" in request.form:
        email = request.form.get('email')

        if User.query.filter_by(email=email).first():
            flash("Cette adresse email est déja utilisée par un autre membre !")
            return redirect(url_for('profile') + '#form-security')

        content = f"""
<html>
    <head>
    </head>
    <body border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#e37b46" style="background: rgb(2,0,36); background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(109,13,125,1) 40%, rgba(10,59,131,1) 100%); padding:20px; color:white; ">
        <h1>Bonjour {current_user.username}</h1>
        <p>Tu as récemment modifié ton adresse email sur FurMap.fr.</p>
        <p>Ta nouvelle adresse email est {email}.</p>
        <p>Si tu n'es pas à l'origine de cette demande contacte immédiatement l'administrateur du site afin de sécuriser ton compte.</p>
        <p>Cordialement, l'équipe FurMap !</p>
    </body>
</html>
"""

        plain = f"""
Bonjour {current_user.username},
Tu as récemment modifié ton adresse email sur FurMap.fr.
Ta nouvelle adresse email est {email}.
Si tu n'es pas à l'origine de cette demande contacte immédiatement l'administrateur du site afin de sécuriser ton compte.
Cordialement, l'équipe FurMap !

"""
        mail_sender(content, plain, current_user.email, "FurMap.fr - Changement d'adresse email")

        email_token = hashlib.md5(random_string(64).encode()).hexdigest()
        current_user.email = email
        current_user.email_token = email_token
        current_user.is_verified = False
        db.session.commit()

        content = f"""
<html>
    <head>
    </head>
    <body border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#e37b46" style="background: rgb(2,0,36); background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(109,13,125,1) 40%, rgba(10,59,131,1) 100%); padding:20px; color:white; ">
        <h1>Bonjour {current_user.username}</h1>
        <p>Tu as récemment modifié ton adresse email sur FurMap.fr.</p>
        <p>Afin de finaliser ce changement il te faut valider ton adresse email.</p>
        <p>Il te suffit de cliquer ici : <a href="{URL_BASE}{url_for('verify', user_id=current_user.id, email_token=email_token)}"> Click me ! </a></p>
        <p>Cordialement, l'équipe FurMap !</p>
    </body>
</html>
"""

        plain = f"""
        Bonjour {current_user.username},
        Tu as récemment modifié ton adresse email sur FurMap.fr.
        Afin de finaliser ce changement il te faut valider ton adresse email.
        Il te suffit de cliquer ici : {URL_BASE}{url_for('verify', user_id=current_user.id, email_token=email_token)}
        Cordialement, l'équipe FurMap !
"""

        mail_sender(content, plain, email, "FurMap.fr - Changement d'adresse email")

    elif "submit-password" in request.form:
        password = request.form.get('password')
        confirmation = request.form.get('confirmation')
        oldpassword = request.form.get('oldpassword')

        if not check_password_hash(current_user.password, oldpassword):
            flash("L'ancien mot de passe ne correspond pas !")
            return redirect(url_for('profile') + '#form-security')
        if confirmation != password:
            flash("Les mots de passes ne sont pas identiques.")
            return redirect(url_for('profile') + '#form-security')
        if len(password) < 8:
            flash("Le mot de passe doit contenir au moins 8 caractères.")
            return redirect(url_for('profile') + '#form-security')
        if re.search('\d', password) is None:
            flash("Le mot de passe doit contenir au moins 1 minuscule, 1 majuscule et 1 chiffre.")
            return redirect(url_for('profile') + '#form-security')
        if re.search('[A-Z]', password) is None:
            flash("Le mot de passe doit contenir au moins 1 minuscule, 1 majuscule et 1 chiffre.")
            return redirect(url_for('profile') + '#form-security')

        current_user.password = generate_password_hash(password, method='sha256')
        db.session.commit()
    elif "submit-picture" in request.form:
        b64_string = base64.b64encode(request.files['file'].read())
        current_user.profile_picture = b64_string.decode('utf-8')
        db.session.commit()
    elif "delete-place" in request.form:
        place_id = request.form.get('place-id')
        Places.query.filter_by(id=place_id).filter_by(user_id=current_user.id).delete()
        db.session.commit()
    elif "add-place" in request.form:
        number = request.form.get('number')
        name = request.form.get('name')
        code = request.form.get('code')
        city = request.form.get('city')

        adress_to_look_for = number + " " + name + " " + code + " " + city + " France"

        response = requests.get(f"https://maps.googleapis.com/maps/api/geocode/json?address={adress_to_look_for.strip()}&sensor=false&key={GOOGLE_API_KEY}")
        data = response.json()

        lat = data["results"][0]["geometry"]["location"]["lat"] + random.uniform(-0.015, 0.015)
        lon = data["results"][0]["geometry"]["location"]["lng"] + random.uniform(-0.015, 0.015)

        new_place = Places(lat=round(lat, 5), lon=round(lon, 5), user_id=current_user.id)
        db.session.add(new_place)
        db.session.commit()
    elif "submit-delete" in request.form and "delete-account" in request.form:
        Places.query.filter_by(user_id=current_user.id).delete()
        User.query.filter_by(id=current_user.id).delete()
        db.session.commit()
        logout_user()
        return redirect(url_for('home', user_id=get_user()))

    return redirect(url_for('profile'))


@app.route('/look_profile/<int:requested_user_id>')
@login_required
def look_profile(requested_user_id):
    requested_user = User.query.filter_by(id=requested_user_id).first()
    if requested_user is None:
        return redirect(url_for('furmap'))

    places = Places.query.filter_by(user_id=requested_user_id).all() or []

    folium_map = folium.Map(
        location=[48.692054, 6.184417],
        zoom_start=5,
        min_zoom=5,
        max_zoom=15
    )

    marker_cluster = MarkerCluster(name="Furries").add_to(folium_map)

    for place in places:
        folium.Marker(
            location=[float(place.lat), float(place.lon)],
            icon = folium.DivIcon(html=(
            """
                <div class="pin"><b class="pin-text">"""+str(place.id)+"""</b></div>
            """
            )),

        ).add_to(marker_cluster)

    ela = folium.MacroElement().add_to(folium_map)
    ela._template = jinja2.Template("""
        {% macro header(this, kwargs) %}
            <style>
                .pin {
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    width:40px;
                    height:40px;
                    background-color:lightcoral;
                    color: black;
                    padding:3px;
                    border:1px solid black;
                    border-radius:20px 20px 0px 20px;
                    font-size: large; 
                    transform: rotate(45deg);
                    margin-top: -40px;
                    margin-left: -20px;
                }
                .pin-text {
                    transform: rotate(-45deg);
                    flex: 0 0;
                }
            </style>
        {% endmacro %}
    """)

    folium_map = folium_map._repr_html_()

    return render_template('look_profile.html', page="look_profile", user=current_user, requested_user=requested_user, map=folium_map, places=places)


@app.route('/furmap')
@login_required
def furmap():

    FOLIUM_MAP = folium.Map(
        location=[48.692054, 6.184417],
        zoom_start=9,
        min_zoom=3,
        max_zoom=15
    )

    marker_cluster_asso = MarkerCluster(name="Associations").add_to(FOLIUM_MAP)

    associations = Association.query.all()

    for association in associations:

        link = None
        if association.contact_type == "telegram":
            link = '<a href="https://t.me/' + association.contact + '" target="parent">' + association.name + '</a>'
        elif association.contact_type == "discord":
            link = '<a href="https://discordapp.com/users/' + association.contact + '" target="parent">' + association.name + '</a>'
        elif association.contact_type == "email":
            link = '<a href="mailto:"' + association.contact + '" target="parent">' + association.name + '</a>'
        elif association.contact_type == "twitter":
            link = '<a href="https://twitter.com/' + association.contact + '" target="parent">' + association.name + '</a>'
        elif association.contact_type == "autre":
            link = '<a href="' + association.contact + '" target="parent">' + association.name + '</a>'

        if link is not None:
            html = f"""
            <div style="text-align: center;">
                { link }
            </div>
            """

        iframe = folium.IFrame(html,
                                width=170,
                                height=40)

        folium.Marker(
            location=[float(association.lat), float(association.lon)],
            popup = folium.Popup(iframe, max_width=100),
            icon = folium.DivIcon(html=(
            """
                <div class="pin-asso"><b class="pin-text">A</b></div>
            """
            )),

        ).add_to(marker_cluster_asso)

    marker_cluster = MarkerCluster(name="Furries").add_to(FOLIUM_MAP)

    places = db.session.query(Places, User).join(User).all() or []

    coordenadas = []
    for place in places:
        coordenadas.append([float(place[0].lat), float(place[0].lon), place[1].username, place[1].id, place[0].id])

    for place in places:
        html = f"""
        <div style="text-align: center;">
            <a style="" target="parent" href="{URL_BASE}/look_profile/{place[1].id}">{place[1].username}</a>
        </div>
        """

        iframe = folium.IFrame(html,
                                width=170,
                                height=40)
        folium.Marker(
            location=[float(place[0].lat), float(place[0].lon)],
            popup = folium.Popup(iframe, max_width=100),
            icon = folium.DivIcon(html=(
            """
                <div class="pin"><b class="pin-text">"""+str(place[0].id)+"""</b></div>
            """
            )),

        ).add_to(marker_cluster)

    mname = FOLIUM_MAP.get_name()

    el = folium.MacroElement().add_to(FOLIUM_MAP)
    el._template = jinja2.Template("""
        {% macro script(this, kwargs) %}
            """+mname+""".on('dragend zoomend mousemove', function onDragEnd(){

                var e = """+mname+""".getBounds().getEast();
                var w = """+mname+""".getBounds().getWest();
                var n = """+mname+""".getBounds().getNorth();
                var s = """+mname+""".getBounds().getSouth();

                var transcodeded_data = """+str(coordenadas)+""";

                var visibleMarker = {};

                for (var i = 0; i < transcodeded_data.length; i++) {
                    var marker = transcodeded_data[i];
                    if (marker[1] > w && marker[1] < e && marker[0] > s && marker[0] < n){
                        if(marker[2] in visibleMarker){
                            visibleMarker[marker[2]]["points"].push(marker[4]);
                        }else{
                            visibleMarker[marker[2]] = {"points" : [marker[4]], "user_id" : marker[3]};
                        }
                    }
                }               
            
                var html = '<ul class="list-group">';
                for (elem in visibleMarker){
                    html = html + "<tr>"
                    html = html + "<th scope='row' style='word-break: break-all;overflow-wrap: break-word;' >" + visibleMarker[elem]["points"] + "</th>"
                    html = html + '<td>' + elem + '</td>'
                    html = html + '<td><a class="btn btn-primary btn-sm" style="margin-left:25px;" href="/look_profile/' + visibleMarker[elem]["user_id"] +'"><i class="fa-solid fa-id-card"></i></a>'
                    html = html + "</tr>"
                }
                html = html + '</ul>'

                parent.my_var.innerHTML = html;
            });
        {% endmacro %}
    """)

    ela = folium.MacroElement().add_to(FOLIUM_MAP)
    ela._template = jinja2.Template("""
    <script src="https://kit.fontawesome.com/f88b3658f2.js" crossorigin="anonymous"></script>
        {% macro header(this, kwargs) %}
            <style>
                .pin, .pin-asso, .pin-net {
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    width:40px;
                    height:40px;
                    color: black;
                    padding:3px;
                    border:1px solid black;
                    border-radius:20px 20px 0px 20px;
                    font-size: large; 
                    transform: rotate(45deg);
                    margin-top: -40px;
                    margin-left: -20px;
                }
                .pin {
                    background-color:lightcoral !important;
                }
                .pin-asso {
                    background-color:lightblue !important;
                }
                .pin-net {
                    background-color:lightgray !important;
                    width:20px;
                    height:20px;
                    margin-top: -20px;
                    margin-left: -10px;
                }
                .pin-text {
                    transform: rotate(-45deg);
                    flex: 0 0;
                }
                .leaflet-popup {
                    left: -114px !important;
                    top: -120px !important;
                }
                .leaflet-container a.leaflet-popup-close-button {
                    font: 30px/30px Tahoma, Verdana, sans-serif !important;
                    color: red;
                }
                .leaflet-popup-content {
                    width: auto !important;
                }
                .leaflet-popup-content>iframe{
                    height:55px !important;
                }
            </style>
        {% endmacro %}
    """)

    folium.LayerControl().add_to(FOLIUM_MAP)

    html_folium_map = FOLIUM_MAP._repr_html_()

    return render_template('map.html', page="furmap", map=html_folium_map)

@app.route('/')
def home():
    users = User.query.count()
    associations = Association.query.count()
    places = Places.query.count() or 0

    return render_template('home.html', page="home", user_id=get_user(), ldig=random.randint(1, 9), rdig=random.randint(1, 9), places=places, users=users, associations=associations)

@app.route('/', methods=['POST'])
def home_post():

    email = request.form.get('email')
    pseudonyme = request.form.get('pseudonyme') or "nc"
    mail_content = request.form.get('mail_content')

    ldig = request.form.get('ldig')
    rdig = request.form.get('rdig')
    cap = request.form.get('cap')

    try:
        if int(ldig) + int(rdig) == int(cap) and pseudonyme is not "nc":
            msg = EmailMessage()
            msg.set_content(mail_content)
            msg['From'] = email
            msg['To'] = MAIL_SENDER
            msg['Subject'] = f"[CONTACT] - {email} - {pseudonyme}"
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(SMTP_URI, 465, context=context) as server:
                server.login(MAIL_SENDER, MAIL_PASSWORD)
                server.sendmail(email, MAIL_SENDER, msg.as_string())
    except:
        pass

    return render_template('home.html', page="home", user_id=get_user(), ldig=random.randint(1, 9), rdig=random.randint(1, 9))

@app.route('/get_async_map')
def get_async_map():
    FOLIUM_MAP = folium.Map(
        location=[46.990896, 3.162845],
        zoom_start=6,
        min_zoom=5,
        max_zoom=9
    )

    marker_cluster = MarkerCluster(name="Furries").add_to(FOLIUM_MAP)

    places = Places.query.all() or []

    for place in places:
        folium.Marker(
            location=[float(place.lat), float(place.lon)],

        ).add_to(marker_cluster)

    html_folium_map = FOLIUM_MAP._repr_html_()

    return jsonify({'map' : html_folium_map})


@app.route('/assolist')
@login_required
def assolist():
    if not current_user.is_admin:
        return redirect(url_for('home', user_id=get_user()))
    associations = Association.query.all()
    return render_template('assolist.html', page="assolist", user=current_user, associations=associations)


@app.route('/assolist', methods=['POST'])
@login_required
def assolist_post():

    if not current_user.is_admin:
        return redirect(url_for('home', user_id=get_user()))

    if "submit-asso" in request.form:
        name = request.form.get('name')
        contact = request.form.get('contact')
        contact_type = request.form.get('type')
        lat = request.form.get('lat')
        lon = request.form.get('lon')
        new_asso = Association(
            is_visible=True,
            lat=lat,
            lon=lon,
            name=name,
            contact=contact,
            contact_type=contact_type
        )
        db.session.add(new_asso)
        db.session.commit()
    elif "delete-asso" in request.form:
        Association.query.filter_by(id=request.form["asso-id"]).delete()
        db.session.commit()

    associations = Association.query.all()
    return render_template('assolist.html', page="assolist", user=current_user, associations=associations)


@app.route('/faq')
def faq():
    return render_template('faq.html', page="faq", user_id=get_user())


@app.route('/legal')
def legal():
    return render_template('legal.html', page="legal", user_id=get_user())

@app.route('/cookies', methods = ['POST', 'GET'])
def cookies():
    cookie_consent = request.cookies.get('cookie_consent')
    if request.method == 'POST':
        if "yes" in request.form:
            cookie_consent = "yes"
        elif "no" in request.form:
            cookie_consent = "no"

        resp = make_response(render_template('cookies.html', user_id=get_user(), cookie_consent=cookie_consent))
        resp.set_cookie('cookie_consent', cookie_consent, samesite='None', secure=True)
        return resp

    return render_template('cookies.html', page="cookies", user_id=get_user(), cookie_consent=cookie_consent)

@app.route('/gdpr')
def gdpr():
    return render_template('gdpr.html', page="gdpr", user_id=get_user())

@app.route('/changelog')
def changelog():
    return render_template('changelog.html', page="changelog", user_id=get_user())


@app.route('/userlist')
@login_required
def userlist():

    args = request.args

    filter_letter = ""
    if "filter" in args:
        filter_letter = args["filter"]

    if not current_user.is_admin:
        return redirect(url_for('home', user_id=get_user()))

    users = User.query.all()

    if filter_letter == "ver":
        users = User.query.filter(User.is_verified.is_(False)).all()
    elif filter_letter == "res":
        users = User.query.filter(User.is_restricted.is_(True)).all()
    elif filter_letter == "ban":
        users = User.query.filter(User.is_blacklisted.is_(True)).all()

    cleared = []

    for i in users:
        i.noplaces = Places.query.filter_by(user_id=i.id).count()

        nocontact = 0
        if i.telegram is not None and i.telegram.strip() != "":
            nocontact +=1
        if i.discord is not None and i.discord.strip() != "":
            nocontact +=1
        if i.twitter is not None and i.twitter.strip() != "":
            nocontact +=1
        if i.twitternsfw is not None and i.twitternsfw.strip() != "":
            nocontact +=1
        if i.furaffinity is not None and i.furaffinity.strip() != "":
            nocontact +=1
        if i.instagram is not None and i.instagram.strip() != "":
            nocontact +=1
        if i.bluesky is not None and i.bluesky.strip() != "":
            nocontact +=1
        if i.youtube is not None and i.youtube.strip() != "":
            nocontact +=1
        if i.tiktok is not None and i.tiktok.strip() != "":
            nocontact +=1
        if i.twitch is not None and i.twitch.strip() != "":
            nocontact +=1
        if i.linktree is not None and i.linktree.strip() != "":
            nocontact +=1

        i.nocontact = nocontact

        if len(i.description or "") > 0:
            i.desc = "Oui"
        else:
            i.desc = "Non"

        if len(i.profile_picture or "") > 0:
            i.pp = "Oui"
        else:
            i.pp = "Non"

        if filter_letter == "r" and i.noplaces == 0 and i.nocontact == 0:
            cleared.append(i.id)
        elif filter_letter == "j" and i.pp == "Non" and i.desc == "Non" and not (i.noplaces == 0 and i.nocontact == 0):
            cleared.append(i.id)
        elif filter_letter == "v" and not(i.pp == "Non" and i.desc == "Non") and not (i.noplaces == 0 and i.nocontact == 0):
            cleared.append(i.id)
        elif filter_letter in ["", "ver", "res", "ban"]:
            cleared.append(i.id)

    if filter_letter == "dbl":
        for x1 in users:
            for x2 in users:
                if x1.username == x2.username and x1.id != x2.id:
                    logger.error(x1)
                    cleared.append(x1.id)

    users = [x for x in users if x.id  in cleared]

    return render_template('userlist.html', page="userlist", user=current_user, users=users)


@app.route('/userlist', methods=['POST'])
@login_required
def userlist_post():
    if not current_user.is_admin:
        return redirect(url_for('home', user_id=get_user()))

    if "ban-user" in request.form:
        user = User.query.filter_by(id=request.form["user-id"]).first()
        user.is_blacklisted = not user.is_blacklisted
        db.session.commit()
    elif "restrain-user" in request.form:
        user = User.query.filter_by(id=request.form["user-id"]).first()
        user.is_restricted = not user.is_restricted
        db.session.commit()
    elif "delete-user" in request.form:
        Places.query.filter_by(id=request.form["user-id"]).delete()
        db.session.commit()
        User.query.filter_by(id=request.form["user-id"]).delete()
        db.session.commit()
    elif "relance-profil" in request.form:
        user_to_mail = User.query.filter_by(id=request.form["user-id"]).first()

        user_to_mail_noplaces = Places.query.filter_by(user_id=user_to_mail.id).count()

        user_to_mail_nocontact = 0
        if user_to_mail.telegram is not None and user_to_mail.telegram.strip() != "":
            user_to_mail_nocontact +=1
        if user_to_mail.discord is not None and user_to_mail.discord.strip() != "":
            user_to_mail_nocontact +=1
        if user_to_mail.twitter is not None and user_to_mail.twitter.strip() != "":
            user_to_mail_nocontact +=1
        if user_to_mail.twitternsfw is not None and user_to_mail.twitternsfw.strip() != "":
            user_to_mail_nocontact +=1
        if user_to_mail.furaffinity is not None and user_to_mail.furaffinity.strip() != "":
            user_to_mail_nocontact +=1
        if user_to_mail.instagram is not None and user_to_mail.instagram.strip() != "":
            user_to_mail_nocontact +=1
        if user_to_mail.bluesky is not None and user_to_mail.bluesky.strip() != "":
            user_to_mail_nocontact +=1
        if user_to_mail.youtube is not None and user_to_mail.youtube.strip() != "":
            user_to_mail_nocontact +=1
        if user_to_mail.tiktok is not None and user_to_mail.tiktok.strip() != "":
            user_to_mail_nocontact +=1
        if user_to_mail.twitch is not None and user_to_mail.twitch.strip() != "":
            user_to_mail_nocontact +=1
        if user_to_mail.linktree is not None and user_to_mail.linktree.strip() != "":
            user_to_mail_nocontact +=1

        user_to_mail_desc = "Tu n'as pas non plus compléter ta description !"
        if len(user_to_mail.description or "") > 0 :
            user_to_mail_desc = "En revanche tu as complété ta description ! Et nous t'en remercions :)"

        content = f"""
<html>
    <head>
    </head>
    <body border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#e37b46" style="background: rgb(2,0,36); background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(109,13,125,1) 40%, rgba(10,59,131,1) 100%); padding:20px; color:white; ">
        <h1>Bonjour {user_to_mail.username}</h1>
        <p>Nous avons remarqué que tu as créé un compte FurMap et nous t'en remercions, mais <b>tu n'as pas complété ton profil ou indqué de localisation</b>. Ces informations sont importantes pour permettre aux autres furries de te trouver et de te contacter :)</p>
        <p>À cette heure, nous comptons :</p>
        <ul>
            <li><b>{user_to_mail_noplaces}</b> places pour te retrouver !</li>
            <li><b>{user_to_mail_nocontact}</b> réseaux pour te contacter !</li>
        </ul>
        <p>{user_to_mail_desc}</p>
        <p>Tu peux mettre à jour toutes tes informations depuis l'onglet << Mon Profile >> en te connectant sur le FurMap :)</p> 
        <p>Il te suffit de cliquer ici : <a href="{URL_BASE}{url_for('profile')}"> Clike me ! </a></p>
        <p>Cordialement, l'équipe FurMap !</p>
    </body>
</html>
"""

        plain = f"""
Bonjour {user_to_mail.username},
Nous avons remarqué que tu as créé un compte FurMap et nous t'en remercions, mais tu n'as pas complété ton profil ou indqué de localisation. Ces informations sont importantes pour permettre aux autres furries de te trouver et de te contacter :)
À cette heure, nous comptons :

    {user_to_mail_noplaces} places pour te retrouver !
    {user_to_mail_nocontact} réseaux pour te contacter !

{user_to_mail_desc}
Tu peux mettre à jour toutes tes informations depuis l'onglet << Mon Profile >> en te connectant sur le FurMap :)
Il te suffit de cliquer ici : {URL_BASE}{url_for('profile')}
Cordialement, l'équipe FurMap !
"""

        mail_sender(content, plain, user_to_mail.email, "FurMap.fr - Completez votre profil !")
    elif "relance-confirm" in request.form:
        user_to_mail = User.query.filter_by(id=request.form["user-id"]).first()

        email_token = hashlib.md5(random_string(64).encode()).hexdigest()
        user_to_mail.email_token = email_token
        db.session.commit()

        content = f"""
<html>
    <head>
    </head>
    <body border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#e37b46" style="background: rgb(2,0,36); background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(109,13,125,1) 40%, rgba(10,59,131,1) 100%); padding:20px; color:white; ">
        <h1>Bonjour {user_to_mail.username}</h1>
        <p>Nous avons remarqué que tu as créé un compte FurMap et nous t'en remercions, mais tu n'as pas vérifié ton adresse email ! La vérification de l'email est une étape importante, car elle te permet de visualiser la carte et de confirmer aux autres membres que tu as vérifié ton compte !</p>
        <p>Il te suffit de cliquer ici : <a href="{URL_BASE}{url_for('verify', user_id=user_to_mail.id, email_token=email_token)}"> Clike me ! </a></p>
        <p>Cordialement, l'équipe FurMap !</p>
    </body>
</html>
"""

        plain = f"""
Bonjour {user_to_mail.username},
Nous avons remarqué que tu as créé un compte FurMap et nous t'en remercions, mais tu n'as pas vérifié ton adresse email ! La vérification de l'email est une étape importante, car elle te permet de visualiser la carte et de confirmer aux autres membres que tu as vérifié ton compte !
Il te suffit de cliquer ici : {URL_BASE}{url_for('verify', user_id=user_to_mail.id, email_token=email_token)}
Cordialement, l'équipe FurMap !
"""
        mail_sender(content, plain, user_to_mail.email, "FurMap.fr - Vérification de votre compte !")
    elif "validate-user" in request.form:

        user = User.query.filter_by(id=request.form["user-id"]).first()
        user.is_restricted = False
        user.is_verified = True
        db.session.commit()

        content = f"""
<html>
    <head>
    </head>
    <body border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#e37b46" style="background: rgb(2,0,36); background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(109,13,125,1) 40%, rgba(10,59,131,1) 100%); padding:20px; color:white; ">
        <h1>Bonjour {user.username}</h1>
        <p>Un administrateur a validé votre inscription sur FurMap.fr ! Vous pouvez désormais vous connecter en utilisant votre email et votre mot de passe. </p>
        <p>Tu peux maintenant mettre à jour toutes tes informations de profil depuis l'onglet << Mon Profile >> en te connectant sur le FurMap :)</p> 
        <p>Il te suffit de cliquer ici : <a href="{URL_BASE}{url_for('profile')}"> Click me ! </a></p>
        <p>Cordialement, l'équipe FurMap !</p>
    </body>
</html>
"""

        plain = f"""
Bonjour {user.username},
Un administrateur a validé votre inscription sur FurMap.fr ! Vous pouvez désormais vous connecter en utilisant votre email et votre mot de passe.
Tu peux maintenant mettre à jour toutes tes informations de profil depuis l'onglet << Mon Profile >> en te connectant sur le FurMap :)
Il te suffit de cliquer ici : {URL_BASE}{url_for('profile')}
Cordialement, l'équipe FurMap !
"""

        mail_sender(content, plain, user.email, "FurMap.fr - Vérification de votre compte !")

    users = User.query.all()

    for i in users:
        i.noplaces = Places.query.filter_by(user_id=i.id).count()

        nocontact = 0
        if i.telegram is not None and i.telegram.strip() != "":
            nocontact +=1
        if i.discord is not None and i.discord.strip() != "":
            nocontact +=1
        if i.twitter is not None and i.twitter.strip() != "":
            nocontact +=1
        if i.twitternsfw is not None and i.twitternsfw.strip() != "":
            nocontact +=1
        if i.furaffinity is not None and i.furaffinity.strip() != "":
            nocontact +=1
        if i.instagram is not None and i.instagram.strip() != "":
            nocontact +=1
        if i.bluesky is not None and i.bluesky.strip() != "":
            nocontact +=1
        if i.youtube is not None and i.youtube.strip() != "":
            nocontact +=1
        if i.tiktok is not None and i.tiktok.strip() != "":
            nocontact +=1
        if i.twitch is not None and i.twitch.strip() != "":
            nocontact +=1
        if i.linktree is not None and i.linktree.strip() != "":
            nocontact +=1          
        i.nocontact = nocontact

        if len(i.description or "") > 0:
            i.desc = "Oui"
        else:
            i.desc = "Non"

        if len(i.profile_picture or "") > 0:
            i.pp = "Oui"
        else:
            i.pp = "Non"

    return render_template('userlist.html', page="userlist", user=current_user, users=users)



@app.route('/administration')
@login_required
def administration():
    if not current_user.is_admin:
        return redirect(url_for('home', user_id=get_user()))

    return render_template('administration.html', page="administration", user=current_user, locked=os.path.isfile(LOCK_FILE))


@app.route('/administration', methods=['POST'])
@login_required
def administration_post():

    if not current_user.is_admin:
        return redirect(url_for('home', user_id=get_user()))

    if "submit-verouillage" in request.form:
        if os.path.isfile(LOCK_FILE):
            os.remove(LOCK_FILE)
        else:
            try:
                open(LOCK_FILE, 'a').close()
            except OSError:
                print('Failed creating the file')
            else:
                print('File created')
    elif "submit-general-mail" in request.form:

        corps = request.form.get('corps')
        sujet = request.form.get('sujet')

        content = f"""
<html>
    <head>
    </head>
    <body border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#e37b46" style="background: rgb(2,0,36); background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(109,13,125,1) 40%, rgba(10,59,131,1) 100%); padding:20px; color:white; ">
        {corps}
    </body>
</html>
"""

        plain = f""" {corps} """

        emails_list = db.session.query(User.email).all()

        msg = MIMEMultipart('alternative')
        msg['From'] = MAIL_SENDER
        msg['To'] = MAIL_SENDER
        msg['Subject'] = sujet

        mimed_html = MIMEText(content, 'html')
        mimed_plain = MIMEText(plain, 'plain')

        msg.attach(mimed_plain)
        msg.attach(mimed_html)

        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(SMTP_URI, 465, context=context) as server:
            server.login(MAIL_SENDER, MAIL_PASSWORD)

            emails = []
            for itter, email in enumerate(emails_list):
                emails.append(email[0])
                if len(emails) == 95:
                    server.sendmail(MAIL_SENDER, (emails), msg.as_string())
                    emails = []
                if itter == len(emails_list) - 1:
                    server.sendmail(MAIL_SENDER, (emails), msg.as_string())                  

            server.quit()

    return render_template('administration.html', page="administration", user=current_user, locked=os.path.isfile('./app/.lock'))


@app.route('/recherche')
@login_required
def recherche():
    users = User.query.all()
    return render_template('recherche.html', page="recherche", user=current_user, users=users)


@app.route('/recherche', methods=['POST'])
@login_required
def recherche_post():
    users = User.query.all()
    username = request.form.get('username')
    city = request.form.get('city')

    if username:
        city = None
        users = User.query.filter(User.username.contains(username))
    elif city:
        username = None
        adress_to_look_for = city + " France"

        response = requests.get(f"https://maps.googleapis.com/maps/api/geocode/json?address={adress_to_look_for.strip()}&sensor=false&key={GOOGLE_API_KEY}")
        data = response.json()

        lat = data["results"][0]["geometry"]["location"]["lat"]
        lon = data["results"][0]["geometry"]["location"]["lng"]

        engine = create_engine(DB_NAME)

        with engine.connect() as con:
            users = []

            #            \****__              ____
            #          |    *****\_      --/ *\-__
            #          /_          (_    ./ ,/----'
            #            \__         (_./  /
            #               \__           \___----^__
            #                _/   _                  \
            #         |    _/  __/ )\"\ _____         *\
            #         |\__/   /    ^ ^       \____      )
            #          \___--"                    \_____ )
            #                                           "
            resultdesesmorts = con.execute(text(f"""SELECT user.id, user.username, user.telegram, user.twitter, places.lat, places.lon,
            ROUND(((2 * asin(sqrt((((sin(RADIANS(places.lat)-RADIANS({lat}))/2)*(sin(RADIANS(places.lat)-RADIANS({lat}))/2)) + cos(RADIANS(places.lat)) * cos(RADIANS({lat})) * ((sin(RADIANS(places.lon)-RADIANS({lon}))/2)*(sin(RADIANS(places.lon)-RADIANS({lon}))/2)))))) * 6371), 3) AS distance
            FROM user CROSS JOIN places ON user.id = places.user_id ORDER BY distance ASC """))

            for row in resultdesesmorts:
                users.append(row)

    return render_template('recherche.html', page="recherche", user=current_user, users=users, username=username, city=city)


@app.route('/sitemap.xml', methods=['GET'])
def sitemap():
    response = make_response(open('./app/sitemap.xml').read())
    response.headers["Content-type"] = "text/plain"
    return response


@app.route('/robots.txt', methods=['GET'])
@app.route('/robot.txt', methods=['GET'])
@app.route('/Robots.txt', methods=['GET'])
@app.route('/Robot.txt', methods=['GET'])
def robot():
    response = make_response(open('./app/robots.txt').read())
    response.headers["Content-type"] = "text/plain"
    return response


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', user_id=None), 404


@app.errorhandler(500)
def server_error(e):
    return render_template('500.html', user_id=None), 500


@app.errorhandler(403)
def not_authorized(e):
    return render_template('404.html', user_id=None), 403
