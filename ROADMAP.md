# FurMap - RoadMap

## 3.1.0
- ~~Ajouter une page "Liste des Associations"~~
- ~~Améliorer la SEO~~
- ~~Changer les images wer un format plus "web-friendly"~~
- ~~Faible nombre de mots sur acceuil~~

## 3.2.0
- ~~Déplacer la génération des listes (user et asso) de map dans le backoffice pour améliorer les perfromances~~

## 4.0.0
- ~~Passer de Google Maps à Open Street Map~~
- Trouver un Géocoding open-source

## 5.0.0
- Adapter la map pour l'europe
- Adapter la map pour le monde

-----  -----  -----

Some words from the page title are not used within the pages content
There are only 454 words on this page. Good pages should have about 800 words of useful content.

There are only a few social sharing widgets on the page. Make your website popular in social networks with social sharing widgets.

There are too few (9) internal links on this page.

This page has only a few links from other websites.
This page only has backlinks from 2 referring domains.
This page only has 2 backlinks.
This page only has few backlinks from 2 different ip addresses.



Es wurden 9 interne Links gefunden.
Man sollte auf mehr als 10 interne Seiten verlinken.

Es wurden 3 ausgehende Links gefunden.
Man sollte auf gute, themenrelevante Seiten verlinken.

Anzahl aller Backlinks auf die Hauptdomain: 0

Anzahl der Backlinks von unterschiedlichen Domains auf die Hauptdomain: 0

Keine Wikipedia Backlinks gefunden.

Es wurde eine noarchive Angabe gefunden. Google wird keine Kopie der Seite im Cache speichern.

Es wurde 1 Inline JavaScript Bereich im HTML Dokument gefunden.
Man sollte diesen in eine externe JavaScript Datei auslagern.

Kein hreflang Tag gefunden.

Die Seite ist unter 4 URLs zu erreichen.
Davon 1 mal direkt und 3 mal per Weiterleitung.
